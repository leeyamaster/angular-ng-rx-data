import { EntityMetadataMap } from '@ngrx/data';

const entityMetadata: EntityMetadataMap = {
  Todo: {},
};

// because the plural of "hero" is not "heros"
const pluralNames = { Todo: 'Todos' };

export const entityConfig = {
  entityMetadata,
  pluralNames
};
