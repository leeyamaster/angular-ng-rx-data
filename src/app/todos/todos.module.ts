import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { TodoService } from './todo.service';
import { TodosComponent } from './todos/todos.component';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzGridModule } from 'ng-zorro-antd/grid';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: TodosComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NzListModule,
    NzButtonModule,
    NzGridModule,
    NzMessageModule,
    RouterModule.forChild(routes)
  ],
  exports: [TodosComponent],
  declarations: [TodosComponent],
  providers: [TodoService]
})
export class TodosModule {}
