import {Component, OnInit} from '@angular/core';
import {TodoService} from '../todo.service';
import {NzMessageService} from "ng-zorro-antd/message";
import {Todo, addTodo} from "../../core/model/todo";

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
  data: Todo[] = [

  ];
  constructor(private todoService: TodoService,
              public msg: NzMessageService) {
    todoService.entities$.subscribe(res=>{
      this.data = res
    });
    todoService.loading$.subscribe(res=>{
      console.log('loading', res)
    });
  }

  ngOnInit() {
    const id = this.msg.loading('Get Todos Data in progress..', { nzDuration: 0 }).messageId;
    this.getTodos();
    this.msg.remove(id);
  }

  add() {
    const todo: addTodo = {
      id: null,
      name: "1",
      title: "2"
    }
    // @ts-ignore
    this.todoService.add(todo);
  }

  delete(todo: Todo) {
    this.todoService.delete(todo);
    this.msg.success('delete success')
  }

  getTodos() {
    this.todoService.getAll();
  }

  update(todo: Todo) {
    this.todoService.update(todo);
  }
}
